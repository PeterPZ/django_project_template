# django项目模板

#### 介绍
一个django项目的架构模板参考

#### 软件架构
```
软件架构说明
.
├── README.md
├── django_project_template  创建项目路径
│   ├── __init__.py
│   ├── django_project_template  项目文件所属路径
│   │   ├── __init__.py
│   │   ├── apps  应用路径
│   │   │   ├── __init__.py
│   │   │   └── testview  测试应用
│   │   │       ├── __init__.py
│   │   │       ├── admin.py
│   │   │       ├── apps.py
│   │   │       ├── migrations
│   │   │       │   └── __init__.py
│   │   │       ├── models.py
│   │   │       ├── tests.py
│   │   │       ├── urls.py
│   │   │       └── views.py
│   │   ├── db.sqlite3
│   │   ├── libs  第三方工具包路径
│   │   │   └── __init__.py
│   │   ├── media  媒体路径
│   │   │   └── uploads  上传文件用文件夹
│   │   ├── settings  设置目录
│   │   │   ├── __init__.py
│   │   │   ├── dev.py  开发环境设置
│   │   │   └── prod.py  生产环境设置
│   │   ├── templates  模板目录
│   │   │   └── test_index.html
│   │   ├── urls.py
│   │   ├── utils  主要工具目录
│   │   │   ├── __init__.py
│   │   │   ├── __pycache__
│   │   │   │   ├── __init__.cpython-36.pyc
│   │   │   │   └── models.cpython-36.pyc
│   │   │   ├── models.py
│   │   │   └── pagination.py
│   │   └── wsgi.py
│   └── manage.py  创建项目路径下主程序入口
└── front  前端文件
    ├── js
    └── static
        ├── css
        ├── html
        └── images
```
#### 安装教程

1. 查看django命令：django-admin 即可显示django的可用命令
2. 创建项目命令：django-admin startproject [项目名称] 若没有报错则创建项目成功
3. 查看manage.py常用命令：python manage.py 即可显示manage中的可用命令
4. 启动项目服务器并修改端口：python manage.py runserver 9999 使用9999端口来启动服务器
5. 创建命令为：python manage.py startapp [项目名称]
6. 创建完成后将应用名称添加到settings.py中的INSTALLED_APPS中完成应用的创建
7. 创建数据迁移命令：python manage.py makemigrations [应用名称] 进行数据迁移的准备步骤，输入以下命令完成数据迁移操作
8. 开始数据迁移命令：python manage.py migrate
`注：如果不输入应用名称则默认对django中的所有应用进行数据迁移操作`
9. 数据迁移完成后使用以下命令进行SQL语句查询
命令为：python manage.py sqlmigrate [应用名称] [文件id]
10. 给django创建一个超级用户命令为：python manage.py createsuperuser

#### 使用说明

1. 将django默认架构进行了更改变换，在项目文件目录下新增目录：apps、templates、libs、utils、settings
2. settings中新增prod.py 和 dev.py 分别用于生产环境和开发环境的配置。在manage.py中进行相应的更改
```
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_project_template.settings.dev')
```
3. 在配置文件中新加apps目录的默认搜索路径
```
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))
```
4. 创建新应用方式变为进入到apps目录下 python3 ../../manage.py startapp [应用名称]
5. 项目现用dev.py配置文件并已开启debug，可以执行命令直接运行服务
![设置DEBUG=True](https://images.gitee.com/uploads/images/2018/1226/143210_eae3872f_1954466.jpeg "1545805912550.jpg")
6. 在主目录下中的urls.py中解除注释路由后开启服务指向指定测试路由/testview/test即可以看到渲染模板文件
![渲染模板文件](https://images.gitee.com/uploads/images/2018/1226/143355_c1d2a0f7_1954466.jpeg "1545806019196.jpg")

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

