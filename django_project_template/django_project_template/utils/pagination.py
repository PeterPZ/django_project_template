# -*- coding: utf-8 -*-
"""
__author__ = 'peter'
__mtime__ = '2018/12/26'
# Follow the master,become a master.
             ┏┓       ┏┓
            ┏┛┻━━━━━━━┛┻┓
            ┃    ☃      ┃
            ┃  ┳┛   ┗┳  ┃
            ┃     ┻     ┃
            ┗━┓       ┏━┛
              ┃       ┗━━━━┓
              ┃ 神兽保佑     ┣┓
              ┃　永无BUG！   ┏┛
              ┗┓┓┏━━━┳┓┏━━━┛
               ┃┫┫   ┃┫┫
               ┗┻┛   ┗┻┛
"""
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    # 默认每页数量
    page_size = 2

    # 指明前端可以通过page_size参数 说明每页数量
    page_size_query_param = 'page_size'

    # 前端指明的每日数量最大极限
    max_page_size = 20
